function versetRandom (){
    $.getJSON("https://quotes.rest/bible/verse.json", function(data) {
      $("#verset").html(data.contents.verse);
      $("#capitol").html(data.contents.book + " " +data.contents.chapter + " " + data.contents.number);
    });
}

$(document).ready(versetRandom);
